# hwapi-doc

## 1. 教师端查看原卷（2019.1.9）
### 接口修改部分
#### · 特殊关注（get   /statistics/class-results/:id/special）
       学生信息里增加了uid字段（学生id），增加了paper_id字段（试卷id） ———  这两个字段用于整卷id数组api所需的参数；
#### · 成绩单模块（学生成绩）  （get   /statistics/class-results/:id/ranking）
       学生信息里增加了paper_id字段（试卷id）  ———  用于获取整卷id数组api所需的参数（学生id字段为student_id）；

### 新增接口部分
#### · 获取整卷id数组api（get    /answerimage/full/ids  ?  student_id=  &  paper_id=）
student_id和paper_id为上述接口新增的字段
   
    response 示例
    {
      "image_uuids": [
        "4305110b-fda0-4768-a8bb-9107ceb87369"
      ]
    }
    
#### · 教师端对应家长端接口
<1> （get  /answerimage/:id/full ? id=） 对应于家长端 （get  /answerimage/:id/full/parent ? id= & student_id=）;

<2> （get  /papers/:uuid） 对应于家长端 （get  /papers/parent/:uuid）